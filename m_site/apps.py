from django.apps import AppConfig

class MSiteConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'm_site'
