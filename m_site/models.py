from django.contrib.auth.models import User
from django.db import models

class UserProfile(models.Model):
    user = models.OneToOneField(User, on_delete=models.CASCADE)
    student_id = models.CharField(max_length=255, blank=True, null=True)
    phone = models.CharField(max_length=255, blank=True, null=True)
    v_token = models.IntegerField(blank=True, null=True)
    matric_no = models.CharField(max_length=255, blank=True, null=True)
    graduation_date = models.CharField(max_length=255, blank=True, null=True)
    id_card = models.ImageField(upload_to='images', blank=True, null=True)
    status = models.IntegerField(default=0, blank=False, null=False)
    @property
    def fullname(self):
        return self.user.first_name+" "+self.user.last_name
    @property
    def email(self):
        return self.user.email
    @property
    def username(self):
        return self.user.username
    @property
    def id(self):
        return self.user.id
    def __str__(self):
        return self.user.first_name

class PartnersModel(models.Model):
    user = models.OneToOneField(User, on_delete=models.CASCADE)
    partner_id = models.CharField(max_length=255, blank=True, null=True)
    phone = models.CharField(max_length=255, blank=True, null=True)
    brandname = models.CharField(max_length=255, blank=True, null=True)
    address = models.CharField(max_length=255, blank=True, null=True)
    payment_channel = models.CharField(max_length=255, blank=True, null=True)
    public_key = models.CharField(max_length=255, blank=True, null=True)
    v_token = models.IntegerField(blank=True, null=True)
    status = models.IntegerField(default=0, blank=False, null=False)
    @property
    def fullname(self):
        return self.user.first_name+" "+self.user.last_name
    @property
    def email(self):
        return self.user.email
    @property
    def username(self):
        return self.user.username
    @property
    def id(self):
        return self.user.id
    def __str__(self):
        return self.user.first_name

class Products(models.Model):
    partner = models.ForeignKey('UserProfile', on_delete=models.CASCADE)
    product_id = models.IntegerField(blank=True, null=True)
    product_name = models.CharField(max_length=255, blank=True, null=True)
    product_image = models.ImageField(upload_to='images', blank=True, null=True)
    price = models.PositiveIntegerField()
    description = models.CharField(max_length=40)

class Orders(models.Model):
    STATUS =(
        ('Pending','Pending'),
        ('Order Confirmed','Order Confirmed'),
        ('Out for Delivery','Out for Delivery'),
        ('Delivered','Delivered'),
    )
    customer=models.ForeignKey('UserProfile', on_delete=models.CASCADE,null=True)
    product=models.ForeignKey('Products',on_delete=models.CASCADE,null=True)
    email = models.CharField(max_length=50,null=True)
    address = models.CharField(max_length=500,null=True)
    mobile = models.CharField(max_length=20,null=True)
    order_date= models.DateField(auto_now_add=True,null=True)
    status=models.CharField(max_length=50,null=True,choices=STATUS)

class Feedback(models.Model):
    name=models.ForeignKey('UserProfile', on_delete=models.CASCADE,null=True)
    feedback=models.CharField(max_length=500)
    date= models.DateField(auto_now_add=True,null=True)

    