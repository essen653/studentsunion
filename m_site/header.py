from django.http import JsonResponse

class CorsMiddleware:
    def __init__(self, get_response):
        self.get_response = get_response

    def __call__(self, request):
        if request.path == '/memberSignu':
            allowed_domain = 'https://api.studentsunion.com.ng/'
            # allowed_domain = 'http://127.0.0.1:8000/'

            # Check the 'Origin' header in the request
            origin = request.headers.get('Origin')

            if origin == allowed_domain:
                response = self.get_response(request)
                response['Access-Control-Allow-Origin'] = origin
                response['Access-Control-Allow-Methods'] = 'GET, POST, PUT, PATCH, DELETE, OPTIONS'
                response['Access-Control-Allow-Headers'] = 'Content-Type, Authorization'
                return response
            else:
                return JsonResponse({'message': 'Unauthorized access'}, status=403)

        # If the request is not for 'memberSignup', proceed without CORS restrictions
        return self.get_response(request)
