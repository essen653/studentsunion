from django import forms
from django.contrib.auth.models import User
from . import models

class ProductForm(forms.ModelForm):
    class Meta:
        model = models.Products
        fields = ['partner','product_name','price','description','product_image']

