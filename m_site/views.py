from django.core.files.storage import default_storage
from django.core.files.base import ContentFile
from django.contrib.auth.models import User, auth
from django.views.decorators.csrf import csrf_exempt
from django.http import JsonResponse
from .models import UserProfile, PartnersModel
from django.template.loader import render_to_string
from django.contrib.auth.hashers import make_password
from django.core.mail import send_mail
import random
from django.middleware.csrf import get_token
from rest_framework.views import APIView


def generate_student_id():
    # Generate a 6-digit unique student ID
    while True:
        student_id = str(random.randint(100000, 999999))
        if not UserProfile.objects.filter(student_id=student_id).exists():
            return student_id

def generate_partner_id():
    # Generate a 6-digit unique student ID
    while True:
        partner_id = str(random.randint(100000, 999999))
        if not PartnersModel.objects.filter(partner_id=partner_id).exists():
            return partner_id

@csrf_exempt
def memberSignup(request):
    if request.method == 'POST':
        username = request.POST.get('username')
        firstname = request.POST.get('firstname')
        lastname = request.POST.get('lastname')
        email = request.POST.get('email')
        password = request.POST.get('password')
        phone = request.POST.get('phone')
        matric_no = request.POST.get('matric_no')
        graduation_date = request.POST.get('graduation_date')
        id_card = request.FILES.get('file')
        student_id = generate_student_id()

        if not email or not username or not password or not firstname or not lastname or not phone or not matric_no or not graduation_date or not id_card:
            return JsonResponse({
                'message': 'Missing parameters',
                'statuscode': 99
                })
        
        if User.objects.filter(email=email).exists():
            return JsonResponse({
                'message': 'Sorry, email is already in use',
                'statuscode': 99
                })
        elif User.objects.filter(username=username).exists():
            return JsonResponse({
                'message': 'Sorry, username is already in use',
                'statuscode': 99
                })
        else:
            user = User.objects.create_user(email=email, first_name=firstname, last_name=lastname, username=username, password=password)
            file_name = f'id_card_{user.id}_{id_card.name}'
            file_path = default_storage.save(file_name, ContentFile(id_card.read()))
            profile = UserProfile.objects.create(user=user, v_token=0, phone=phone, matric_no=matric_no, graduation_date=graduation_date, id_card=file_path, student_id=student_id)
            user.save()
            profile.save()
            send_welcome_email(email, firstname)
            return JsonResponse({
                'message': 'Successful',
                'statuscode': 0,
                'id': profile.student_id
                })
        
    else:
        return JsonResponse({
            'message': 'Invalid request method',
            'statuscode': 99
            })

@csrf_exempt
def partnerSignup(request):
    if request.method == 'POST':
        email = request.POST.get('email')
        username = request.POST.get('username')
        phone = request.POST.get('phone')
        password = request.POST.get('password')
        brandName = request.POST.get('brandname')
        address = request.POST.get('address')
        firstname = request.POST.get('firstname')
        lastname = request.POST.get('lastname')
        paymentChannel = request.POST.get('channel')
        apiKey = request.POST.get('api')

        if not email or not username or not password or not brandName or not address or not firstname or not paymentChannel or not lastname or not apiKey:
            return JsonResponse({'message': 'Missing parameters'})
        else:
            if User.objects.filter(email=email).exists():
                return JsonResponse({'message': 'Sorry, email is already in use'})
            elif User.objects.filter(username=username).exists():
                return JsonResponse({'message': 'Sorry, username is already in use'})
            else:
                user = User.objects.create_user(email=email, first_name=firstname, last_name=lastname, username=username, password=password)
                user.is_staff = True
                partner_id = generate_partner_id()
                profile = PartnersModel.objects.create(user=user, partner_id=partner_id, phone=phone, brandname=brandName, address=address, payment_channel=paymentChannel, public_key=apiKey)
                profile.save()
                user.save()
                send_partner_welcome_email(email, firstname)
                return JsonResponse({
                    'message': 'Successful',
                    'statuscode': 0
                    })
    else:
        return JsonResponse({'message': 'Invalid request method'})


def send_welcome_email(email, firstname):
    subject = 'Welcome to Student Union'
    message = 'Thank you for signing up. We are excited to have you on board!'
    from_email = 'noreply@studentsunion.com.ng'
    recipient_list = [email]
    
    html_message = render_to_string('welcome.html', {'firstname': firstname})

    send_mail(
        subject,
        message,
        from_email,
        recipient_list,
        fail_silently=False,
        html_message=html_message,
    )

def send_partner_welcome_email(email, firstname):
    subject = 'Welcome to Student Union'
    message = 'Thank you for signing up. We are excited to have you on board!'
    from_email = 'noreply@studentsunion.com.ng'
    recipient_list = [email]
    
    html_message = render_to_string('welcome_partner.html', {'firstname': firstname})

    send_mail(
        subject,
        message,
        from_email,
        recipient_list,
        fail_silently=False,
        html_message=html_message,
    )

@csrf_exempt
def login(request):
    if request.method == 'POST':
        if 'username' not in request.POST or 'password' not in request.POST:
            return JsonResponse({'message': 'Missing parameters - username and password are required.'}, status=400)
        
        username = request.POST['username']
        password = request.POST['password']
        
        user = auth.authenticate(username=username, password=password)
        
        if user is not None:
            auth.login(request, user)
            csrf_token = get_token(request)
            
            if user.is_superuser == True:
                # User is a superuser
                return JsonResponse({
                    'message': 'Successful - Superuser',
                    'userid': user.id,
                    'csrf_token': csrf_token,
                    'statuscode': 0
                    })
            else:
                if user.is_staff == False:
                    # User login
                    return JsonResponse({
                        'message': 'User logged in successfully',
                        'userid': user.id,
                        'csrf_token': csrf_token,
                        'statuscode': 1
                        })
                elif user.is_staff == True:
                    # Partner login
                    return JsonResponse({
                        'message': 'Partner logged in successfully',
                        'userid': user.id,
                        'csrf_token': csrf_token,
                        'statuscode': 2
                        })
                else :
                    return JsonResponse({
                        'message': 'Unknown error',
                        'statuscode': 99
                        })
        else:
            return JsonResponse({
                'message': 'Invalid username or password.',
                'statuscode': 99
                })
            
    else:
        return JsonResponse({
            'message': 'Invalid request method'}, status=403)

from django.shortcuts import get_object_or_404
from django.http import JsonResponse
from django.middleware.csrf import get_token
from django.contrib.auth.decorators import login_required

# @login_required
def userDetails(request):
    if request.method == 'GET':
        # csrf_token = request.GET.get('csrftoken')
        userid = request.GET.get('userid')

        if not userid:
            return JsonResponse({'message': 'userid is required.'}, status=400)

        # if csrf_token != get_token(request):
        #     return JsonResponse({'message': 'Invalid CSRF token.'}, status=403)

        # Retrieve user details from both tables
        try:
            user = get_object_or_404(User, id=userid)
            user_profile = get_object_or_404(UserProfile, user=user)
        except User.DoesNotExist:
            return JsonResponse({'message': 'User not found.', 'statuscode': 99,}, status=404)
        except UserProfile.DoesNotExist:
            return JsonResponse({'message': 'User profile not found.'}, status=404)

        # Prepare response
        response_data = {
            'statuscode': 0,
            'user_details': {
                'userid': user.id,
                'username': user.username,
                'email': user.email,
                'first_name': user.first_name,
                'last_name': user.last_name,
                'student_id': user_profile.student_id,
                'phone': user_profile.phone,
                'matric_no': user_profile.matric_no,
                'graduation_date': user_profile.graduation_date,
                'id_card_url': user_profile.id_card.url if user_profile.id_card else None,
                'status': user_profile.status,
            }
        }

        return JsonResponse(response_data, status=200)
    else:
        return JsonResponse({'message': 'Invalid request method.'}, status=403)


@csrf_exempt
def index(request):
    return JsonResponse({'message': 'Unauthorized access'}, status=403)

@csrf_exempt
def approveMember(request):
    if request.method == 'GET':
        adminID = request.GET.get('adminid')
        memberid = request.GET.get('memberid')

        if not adminID:
            return JsonResponse({'message': 'Please login'})
        
        try:
            admin_user = User.objects.get(id=adminID)
        except User.DoesNotExist:
            return JsonResponse({'message': 'Admin does not exist'})

        if not admin_user.is_superuser:
            return JsonResponse({'message': 'Unauthorized access'})

        try:
            member_to_approve = User.objects.get(id=memberid)
        except User.DoesNotExist:
            return JsonResponse({'message': 'Member does not exist'})

        # Set is_staff to 1 for the user to approve
        member_to_approve.is_staff = 1
        member_to_approve.save()
        
        return JsonResponse({'message': 'Successfully approved'})

    else:
        return JsonResponse({'message': 'Invalid request method'})

def approvePartner(request):
    if request.method == 'GET':
        adminID = request.GET.get('adminid')
        memberid = request.GET.get('memberid')

        if not adminID:
            return JsonResponse({'message': 'Please login'})
        
        try:
            admin_user = User.objects.get(id=adminID)
        except User.DoesNotExist:
            return JsonResponse({'message': 'Admin does not exist'})

        if not admin_user.is_superuser:
            return JsonResponse({'message': 'Unauthorized access'})

        try:
            member_to_approve = User.objects.get(id=memberid)
        except User.DoesNotExist:
            return JsonResponse({'message': 'Partner does not exist'})

        # Set is_staff to 1 for the user to approve
        member_to_approve.is_staff = 3
        member_to_approve.save()
        
        return JsonResponse({'message': 'Successfully approved'})

    else:
        return JsonResponse({'message': 'Invalid request method'})

def randomNumber(minimum, maximum):
    return random.randint(minimum, maximum)

@csrf_exempt
def invalid_endpoint(request, *args, **kwargs):
    return JsonResponse({'message': 'Invalid endpoint'}, status=404)

def send_token(email, firstname, link, token):
    subject = 'Request Password Reset'
    message = 'Password reset token was just generated for you'
    from_email = 'noreply@studentsunion.com.ng'
    recipient_list = [email]
    
    html_message = render_to_string('reset_token.html', {'firstname': firstname, 'token': token, 'link': link})

    send_mail(
        subject,
        message,
        from_email,
        recipient_list,
        fail_silently=False,
        html_message=html_message,
    )

def notifyPasswordChanged(email, firstname):
    subject = 'Password Change Successful'
    message = 'Your password was successfully changed'
    from_email = 'noreply@studentsunion.com.ng'
    recipient_list = [email]
    
    html_message = render_to_string('password_changed.html', {'firstname': firstname})

    send_mail(
        subject,
        message,
        from_email,
        recipient_list,
        fail_silently=False,
        html_message=html_message,
    )



@csrf_exempt
def forgotPassword(request):
    if request.method == 'POST':
        email = request.POST.get('email')
        if not email:
            return JsonResponse({'message': 'Missing parameters - email is required.'}, status=400)

        user = User.objects.filter(email=email).first()
        firstname = user.first_name
        if not user:
            return JsonResponse({'message': 'Invalid email address'})

        token = randomNumber(1000, 9999)
        user_id = user.id
        auth = UserProfile.objects.filter(user_id=user_id).first()

        if auth is not None:
            auth.v_token = token
            auth.save()
            firstname = user.first_name
            link = f'http://studentsunion.com.ng/reset-password?email={email}&token={token}'
            send_token(email, firstname, link, token)
            return JsonResponse({
                'message': 'Successful',
                'statuscode': 0
                })
        else:
            part = PartnersModel.objects.filter(user_id=user_id).first()
            part.v_token = token
            part.save()
            link = f'http://studentsunion.com.ng/reset-password?email={email}&token={token}'
            send_token(email, firstname, link, token)
            return JsonResponse({
                'message': 'Successful',
                'statuscode': 0
                })
    else:
        return JsonResponse({'message': 'Invalid request method'})

@csrf_exempt
def resetPassword(request):
    if request.method == 'POST':
        new_password = request.POST.get('password')
        token = request.POST.get('token')
        email = request.POST.get('email')
        auth = UserProfile.objects.filter(user__email=email, v_token=token).first()
        if not auth:
            part = PartnersModel.objects.filter(user__email=email, v_token=token).first()
            if not part:
                return JsonResponse({'message': 'Invalid email or token'})
            
            part.user.password = make_password(new_password)
            part.user.save()
            firstname = part.user.first_name
            part.v_token = 0
            part.save()
            notifyPasswordChanged(email, firstname)
            return JsonResponse({
                'message': 'Successful',
                'statuscode': 0
                })
        else:
            auth.user.password = make_password(new_password)
            auth.user.save()
            firstname = auth.user.first_name
            auth.v_token = 0
            auth.save()
            notifyPasswordChanged(email, firstname)
            return JsonResponse({
                'message': 'Successful',
                'statuscode': 0
                })
    else:
        return JsonResponse({'message': 'Invalid request method'})