from django.urls import path
from .views import forgotPassword, memberSignup, userDetails, resetPassword, login, partnerSignup, approveMember, invalid_endpoint, index
from django.conf import settings
from django.conf.urls.static import static
from .partners import add_product, check

urlpatterns = [
    path('register', memberSignup, name='register'),
    path('login', login, name='login'),
    path('', index, name='index'),
    path('partnerSignup', partnerSignup, name='partnerSignup'),
    path('approveMember', approveMember, name='approveMember'),
    path('<path:invalid_path>/', invalid_endpoint),
    path('forgot-password', forgotPassword, name='forgot-password'),
    path('reset-password', resetPassword, name='reset-password'),
    path('add-product', add_product, name='add-product'),
    path('user-details', userDetails, name='user-details'),
    # path('user-details/', userDetails, name='user-details/'),
    path('check', check, name='check')
]

if settings.DEBUG:
    urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)