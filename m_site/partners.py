from django.core.files.storage import default_storage
from django.core.files.base import ContentFile
from django.contrib.auth.models import User, auth
from django.views.decorators.csrf import csrf_exempt
from django.http import JsonResponse
from .models import UserProfile, PartnersModel
from django.template.loader import render_to_string
from django.shortcuts import render
from django.core.mail import send_mail
from . import forms, models
from django.middleware.csrf import get_token
from rest_framework.views import APIView

def check(request):
    if request.method == 'GET':
        email = request.GET.get('email')
        if not email:
            return JsonResponse({'message': 'Missing parameters - email is required.'}, status=400)

        user = UserProfile.objects.filter(user__email=email).first()
        if not user:
            partner = PartnersModel.objects.filter(user__email=email).first()
            if not partner:
                return JsonResponse({'message': 'Invalid email address'})
            else:
                name = partner.fullname
                username = partner.username
                return JsonResponse({'name': name, 'username': username})
        else:
            name = user.fullname
            username = user.username
            return JsonResponse({'name': name, 'username': username})

def add_product(request):
    productForm=forms.ProductForm()
    if request.method=='POST':
        productForm=forms.ProductForm(request.POST, request.FILES)
        if productForm.is_valid():
            productForm.save()
            return JsonResponse({
                'message': 'Successful',
                'statuscode': 0
                })
        else:
            return JsonResponse({'message': 'Missing parameters'})
    else:
        return JsonResponse({'message': 'Invalid request method'})


class ResetPassword(APIView):
    
    @csrf_exempt
    def post(request):
        email = request.data.get('email')
        if not email:
            return JsonResponse({'message': 'Missing parameters - email is required.'}, status=400)
        
        user = User.objects.filter(email=email).first()
        if not user:
            return JsonResponse({'message': 'Invalid email address'})
        
        # token = randomNumber(1000, 9999)
        user_id = user.id
        auth = UserProfile.objects.filter(user_id=user_id).first()
        # auth.v_token = token
        auth.save()
        firstname = user.first_name
        if not auth:
            part = PartnersModel.objects.filter(user_id=user_id).first()
            # part.v_token = token
            part.save()
            link = f'http://studentsunion.com.ng/reset-password?email={email}&token={token}'
            # send_token(email, firstname, link, token)
            return JsonResponse({
                'message': 'Successful',
                'statuscode': 0
                })
        
        link = f'http://studentsunion.com.ng/reset-password?email={email}&token={token}'
        # send_token(email, firstname, link, token)
        return JsonResponse({
            'message': 'Successful',
            'statuscode': 0
            })

    @csrf_exempt
    def put(request):
        new_password = request.data.get('password')
        token = request.data.get('token')
        email = request.data.get('email')
        auth = UserProfile.objects.filter(user__email=email, v_token=token).first()
        if not auth:
            part = PartnersModel.objects.filter(user__email=email, v_token=token).first()
            if not part:
                return JsonResponse({'message': 'Invalid email or token'})
            
            # part.user.password = make_password(new_password)
            part.user.save()
            firstname = part.user.first_name
            part.v_token = 0
            part.save()
            # notifyPasswordChanged(email, firstname)
            return JsonResponse({
                'message': 'Successful',
                'statuscode': 0
                })
        
        # auth.user.password = make_password(new_password)
        auth.user.save()
        firstname = auth.user.first_name
        auth.v_token = 0
        auth.save()
        # notifyPasswordChanged(email, firstname)
        return JsonResponse({
            'message': 'Successful',
            'statuscode': 0
            })
   