# Student Union Python API

## Getting started

### Clone this repository

```sh
git clone git@gitlab.com:essen653/studentsunion.git
```

### Install Dependencies

```
Django==3.2.5
requests==2.26.0
```

# API Documentation

This document provides an overview of the endpoints and status cases for our API.

## Use case
This API uses "statuscode" for responses
```
0: Success
99: Other validation errors
```

## Authentication

Before using the following endpoints, you must authenticate.

**Note: All routes return -message, -code response props**

**Base URI**:
`https://api.studentsunion.com.ng/`

## Authentication Endpoints

### Register a User

**Endpoint**: `POST /register`

**Description**: Register a new user.

**Request Body**:

```json
{
  "username": "User Name",
  "email": "user@example.com",
  "password": "string|alphanumeric",
  "firstname": "string",
  "lastname": "string",
  "phone": "string",
  "matric_no": "string",
  "graduation_date": "DateField",
  "file": "ID Card file upload"
}
```

**Response Statuscode**:
- 0 OK: User registered successfully.
- 99 Other Errors: Validation error.

### Partner Registration

**Endpoint**: `POST /partnerSignup`

**Description**: Register a new partner.

**Request Body**:

```json
{
  "username": "User Name",
  "email": "user@example.com",
  "password": "string|alphanumeric",
  "firstname": "string",
  "lastname": "string",
  "phone": "string",
  "brandname": "string",
  "address": "string",
  "channel": "Payment channel is either paystack or flutterwave",
  "api": "Payment channel publick api key"
}
```

**Response Statuscode**:
- 0 OK: User registered successfully.
- 99 Other Errors: Validation error.

**HTTP Response**:

- 200 OK: User registered successfully.
- 400 Bad Request: Validation errors.
- 500 Internal Server Error: Server error.

### All Users Login both superadmin, partner and student

**Endpoint**: `POST /login`

**Description**: User login to the application.

**Request Body**:

```json
{
  "username": "user@example.com",
  "password": "your_password"
}
```

**Response Statuscode**:

- 0 OK: Admin logged in successfully.
- 1 OK: User logged in successfully.
- 2 OK: Partner logged in successfully.
- 99 Other Errors: Validation Error.

### Forgot Password

**Endpoint**: `POST /forgot-password`

**Description**: Request for token.

**Request Body**:

```json
{
  "email": "user@example.com",
}
```

**Response Statuscode**:
- 0 OK: Token sent to provided email.
- 99 Other Errors: Email not exit.

### Reset Password

**Endpoint**: `PUT /reset-password`

**Description**: Reset password.

**Request Body**:

```json
{
  "email": "user@example.com",
  "token": "integer",
  "password": "New Password",
}
```

**Response Statuscode**:
- 0 OK: Password reset was successful.
- 99 Other Errors: Invalid token or email.

### Users Details

**Endpoint**: `GET /user-details`

**Description**: Authenticated user details.

**Request Body**:

```json
{
  "userid": "authenticated userid",
}
```

**Response Statuscode**:
- 0 OK: Successful.
- 99 Not found: User not found.



This documentation provides detailed information about the available API endpoints and their usage. Please ensure that you provide valid input data and handle responses accordingly.

## Error Handling

The API handles errors with the following HTTP status codes and corresponding responses:

- 404 Not Found: Resource not found.
- 403 Forbidden: Access denied.
- 400 Bad Request: Invalid request.
- 401 Unauthorized: Authentication failure.
- 500 Internal Server Error: Server error.

---

## Usage

To use these endpoints, make HTTP POST requests to the specified URLs with the required headers and request bodies.
